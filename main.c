//Dirichlet on dashed boundary => phi(x) = g(x)
//von Neumann elsewhere => dphi/dn = 0 where n is outward facing normal vector
//if phi[0][2] is a bd point then when calculating phi[1][2] use phi[0][2] = phi[2][2]  for von neumann
//Gauss-Seidel preconditioner is M=D-E where D is diagonal of A and -E is lower triag part of A

#include "functions.h"

int main(int argc, char * argv[])
{
	//for debugging - sleeps so we can attach gdb to the process then change the mydebug var using gdb
	volatile int mydebug = 0;

	printf("program started\n");
	int size, rank;
	char hostname[256];
	MPI_Status stat;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	gethostname(hostname,sizeof(hostname));

	
	printf("PID %d on %s ready for attach\n", getpid(), hostname);
	//fflush(stdout);
	while (0 == mydebug)
		sleep(5);
	//MPI_Barrier(MPI_COMM_WORLD);	
	

	//printf("N = %d\nn = %d\n", N, n);
	//initialise vars 
	b = calloc(N*N, sizeof(double));  
	double * phi = calloc(N * N, sizeof(double)); //8 squares in domain
	double * r = calloc(N * N, sizeof(double));
	double * r_next = calloc(N * N, sizeof(double));
	double * p = calloc(N * N, sizeof(double));
	double * Ax = calloc(N * N, sizeof(double)); //this is divgradphi
	double * Ap = calloc(N * N, sizeof(double));
	double * z = calloc(N * N, sizeof(double));  //init guess is 0
	double * z_next = calloc(N * N, sizeof(double));
	double * minAx = calloc(N * N, sizeof(double));  //for convergence
	//int * truth_table = calloc(N*N, sizeof(int));
	
	int srow = 1, erow = n+1, scol = 1, ecol = N-1;  //start and end rows and cols, excludes halo points. n+1 = N-1 here

	printf("initialising vars\n");	
	int i,j;
	for (i=srow; i<erow; i++)
	{
		printf("i=%d, j=%d\n", i,j);	
		for (j=scol; j<ecol; j++)
		{
//			printf("got here, i = %d, j = %d\n", i, j);	
			phi[i*N + j] = 1; //init guess, ghost rows are uninitialised so if we get nonsense answers the bug could be here
		}
		//init halo rows by sending - tbh these sends and receives are not really necessary here, could just intialise halo points
		if (rank == 0)
		{
			MPI_Send(&phi[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
			printf("rank %d has sent\n", rank);
			MPI_Recv(&phi[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
			printf("rank %d has received\n", rank);
		}
		else if (rank == 1)
		{
			MPI_Send(&phi[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
			printf("rank %d has received\n", rank);
			MPI_Recv(&phi[i*N + 0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
			printf("rank %d has received\n", rank);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		for (j=scol; j<ecol; j++)
		{
			Ax[i*N + j] = Laplacian(phi,i,j, rank); //boundary conditions handled in this function
			r[i*N + j] = b[i*N + j] - Ax[i*N + j];  //precomp
			z_next[i*N + j] = GS(z, z_next, r, i, j, rank); //first iteration of z precomputed to give better starting z
			p[i*N + j] = z_next[i*N + j];
		}
		if (rank == 0)
		{
			MPI_Send(&p[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
			MPI_Recv(&p[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
		}
		else if (rank == 1)
		{
			MPI_Send(&p[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
			MPI_Recv(&p[i*N +0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
		}	
		MPI_Barrier(MPI_COMM_WORLD);
		for (j=scol; j<ecol; j++)
		{
			Ap[i*N + j] = Laplacian(p,i,j, rank);
			minAx[i*N + j] = 10;
		}
		if (rank == 0)
		{
			MPI_Send(&Ap[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
			MPI_Recv(&Ap[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
		}
		else if (rank == 1)
		{
			MPI_Send(&Ap[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
			MPI_Recv(&Ap[i*N +0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
		}
		MPI_Barrier(MPI_COMM_WORLD);
		if (rank == 0)	
		printf("i=%d, j=%d\n", i,j);	
		printf("rank %d finished init\n",rank);	
	}
	printf("past  sendrecv\n");	
	//pointer swap so z is precomputed for while loop
	double * temp = z;
	z = z_next;
	z_next = temp;

	//printf("Ax = %lf\n", Ax[10]);
	//conjugate gradient
	double alpha, beta;
	double truth_sum;
	double minr=10;
	double tol = 1e-3;
	int count = 0;
	//printf("phi = %lf\n", phi[0]);
	while(count < 100000)
	//while(truth_sum < N*N)
//	while (sqrt(dot(r,r)) > tol)
	{
		//print_matrix(truth_table);
		//printf("Ap = %lf\n", Ap[0]);
		//truth_sum = 0;  //reset
		alpha = dot(r,z)/dot(Ap,p);
		//printf("alpha = %lf\n", alpha);
		for (i=1; i<n+1; i++)
		{
			for (j=1; j<n+1; j++)
			{
				//truth_table[i*N + j] = 0; //reset
				phi[i*N + j] = phi[i*N + j] + alpha*p[i*N + j];
				if (rank == 0)
				{
					MPI_Send(&phi[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
					MPI_Recv(&phi[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
				}
				else if (rank == 1)
				{
					MPI_Send(&phi[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
					MPI_Recv(&phi[i*N + 0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
				}	
				Ax[i*N + j] = Laplacian(phi,i,j,rank); //need to do even-odd
				if (fabs(Ax[i*N + j]) < fabs(minAx[i*N + j]) && fabs(Ax[i*N + j]) > 0)  //maybe do if == 0 do again? surely shouldnt be stricly 0
				{
					minAx[i*N + j] = Ax[i*N + j];
					//printf("new minAx = %lf\n", minAx);
				}
				//if (Ax[i*N + j] < tol)
				//	truth_table[i*N + j] = 1;
				r_next[i*N + j] = r[i*N + j] - alpha*Ap[i*N + j];
				if (fabs(r_next[i*N + j]) < fabs(minr))
				{
					minr = r_next[i*N + j];
					//printf("new minr = %lf\n", minr);
				}
				if (rank == 0)
				{
					MPI_Send(&z[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
					MPI_Recv(&z[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
				}
				else if (rank == 1)
				{
					MPI_Send(&z[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
					MPI_Recv(&z[i*N + 0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
				}
				//not sure if these are needed here for z_next	
				if (rank == 0)
				{
					MPI_Send(&z_next[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
					MPI_Recv(&z_next[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
				}
				else if (rank == 1)
				{
					MPI_Send(&z_next[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
					MPI_Recv(&z_next[i*N + 0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
				}	
				z_next[i*N + j] = GS(z, z_next, r_next, i, j, rank);  //changed r to r_next here
			}	 
		}
		//printf("phi = %lf\n", phi[0]);
		//printf("Ax[0] = %lf\n", Ax[0]);
//		printf("r = %lf\n", r_next[0]);
		beta = dot(r_next,z_next)/dot(r,z); 
		//printf("beta = %lf\n", phi[0]);
		for (i=1; i<n+1; i++)
		{
			for (j=1; j<n+1; j++)
			{
				p[i*N + j] = z_next[i*N + j] + beta*p[i*N+j];	
				if (rank == 0)
				{
					MPI_Send(&p[i*N + n],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD);  //send last col to rank 1 halo
					MPI_Recv(&p[i*N + n+1],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 1
				}
				else if (rank == 1)
				{
					MPI_Send(&p[i*N + 1],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);  //send first col to rank 0 halo
					MPI_Recv(&p[i*N + 0],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //recv halo col from rank 0
				}	
				Ap[i*N + j] = Laplacian(p,i,j,rank);
				//truth_sum += truth_table[i*N + j]; //if = N^2 break
				//r[i*N + j] = r_next[i*N + j];
			}
		}
		//printf("p = %lf\n", p[0]);
		
		//pointer swap for next round
		temp = r_next;
		r_next = r;
		r = temp;	
		temp = z_next;
		z_next = z;
		z = temp;	

//		printf("mod(r) = %lf\n", sqrt(dot(r,r)));	
		count++;
		
	}
	//printf("minr = %lf and smallest value of Ax achieved:\n", minr);
	//print_matrix(minAx);
	//printf("Laplacian of solution:\n");
	if (rank == 0)
		print_matrix(Ax);
	if (rank == 1)
		print_matrix(Ap);
	printf("got here, rank = %d\n", rank);	
	printf("address of Ap = %p\n", Ap);	
	printf("address of Ax = %p\n", Ax);	
	printf("address of p = %p\n", p);	
	printf("address of phi = %p\n", phi);	
	printf("address of r = %p\n", r);	
	printf("address of z = %p\n", z);	
	printf("address of b = %p\n", b);	
	//free(truth_table);
	free(Ap);
	free(Ax);	
	free(b);
	free(r);
	free(r_next); //need to free temp instead of r_next cause of swap?
	free(z);
	free(temp);  //not sure if needed?
	free(z_next);
	free(p);
	free(phi);
	free(minAx);
	free(b);
	MPI_Finalize();	
	return 0;
}	
