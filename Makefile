CC = mpicc -O3
CFLAGS = -Wall -g -lm

objects = functions.o

main: main.c functions.h $(objects)
	$(CC) $(CFLAGS) $(objects) main.c

functions.o: functions.h functions.c
	$(CC) $(CFLAGS) -c functions.c

test: main.c functions.h $(objects)
	$(CC) $(CFLAGS) main.c $(objects)
	mpirun ./a.out

valgrind: main.c functions.h $(objects)
	$(CC) $(CFLAGS) main.c $(objects)
	mpirun valgrind -v --leak-check=full --track-origins=yes --gen-suppressions=all  --tool=memcheck ./a.out 
clean:
	rm a.out *.o
