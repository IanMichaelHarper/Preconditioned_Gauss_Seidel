#include "functions.h"

//This function will act as Ax in our algo
double Laplacian(double * phi, int i, int j, int rank)
{
	//need to do even-odd here
	double up, left, right, down;
	if (i == n)
		down = phi[(i-1)*N + j];  //von neumann  
	else
		down = phi[(i+1)*N + j];
	if (i == 1)
	{
		up = phi[(i+1)*N + j]; //von neumann
		/* for full domain here
		if (rank < 3)  //top boundary
			up = phi[(i+1)*N - j];  //Von Neumann here
		else if (rank == 5)
			up = phi[(i+1)*N - j];  //Von Neumann here
		else
			MPI_Recv(&phi[0+1],n,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //receive into halo row - might need to be Irecv, n might be n-1
	*/
	}
	else
		up = phi[(i-1)*N + j];  //internal points
	if (j == n)
	{
		if (rank == 1)
		{
			phi[i*N + j+1] = phi[i*N + j-1]; //set halo point from von Neumann bd cond
			right = phi[i*N + j-1]; //for return value
		}
		else
			right = phi[i*N + j+1];
	}
	else
		right = phi[i*N + j+1];
	if (j == 1)
	{
		if (rank == 0)
		{
			phi[i*N + j-1] = phi[i*N + j+1]; //von neumann
			left = phi[i*N + j+1];
		}
		else
			left = phi[i*N + j-1];
	}
	else
		left = phi[i*N + j-1];

	return (up+left+right+down-4*phi[i*N + j])/(h*h);
}

//dot product
double dot(double * v, double * u)
{
	int i,j;
	double sum = 0;
	for (i=1; i<n+1; i++)
	{
		for (j=1; j<n+1; j++)
		sum += v[i*N + j] * u[i*N + j];
	}
	return sum;	
}

void print_matrix(double * A)
{
	int i,j;
	for (i=0; i<N; i++)
	{
		for (j=0; j<N; j++)
			printf("%lf ", A[i*N + j]);
		printf("\n");
	}
}

//value of z at (i,j) for next iteration
//not sure whether to do a loop in here too get final converged vector or just do 1 iteration and use in main CG loop
double GS(double * z, double * z_next, double * r, int i, int j, int rank)
{
	/*double up, left, right, down;
	if (i == N-1)
		down = 0;  //Dirichlet for the mo
	else
		down = z[(i+1)*N + j];
	if (i == 0)
		up = 0;
	else
		up = z_next[(i-1)*N + j];
	if (j == N-1)
		right = 0;
	else
		right = z[i*N + j+1];
	if (j == 0)
		left = 0;
	else
		left = z_next[i*N + j-1];
	*/

	//need to do even-odd here
	double up, left, right, down;
	if (i == n)
		down = z_next[(i-1)*N + j];  //von neumann  
	else
		down = z[(i+1)*N + j];
	if (i == 1)
	{
		up = z[(i+1)*N + j]; //von neumann
		/* for full domain here
		if (rank < 3)  //top boundary
			up = phi[(i+1)*N - j];  //Von Neumann here
		else if (rank == 5)
			up = phi[(i+1)*N - j];  //Von Neumann here
		else
			MPI_Recv(&phi[0+1],n,MPI_DOUBLE,0,0,MPI_COMM_WORLD,&stat); //receive into halo row - might need to be Irecv, n might be n-1
	*/
	}
	else
		up = z_next[(i-1)*N + j];  //internal points
	if (j == n)
	{
		if (rank == 1)
		{
			//not sure if both should be z_next here?
			z[i*N + j+1] = z_next[i*N + j-1]; //set halo point from von Neumann bd cond
			right = z_next[i*N + j-1]; //for return value
		}
		else
			right = z[i*N + j+1];
	}
	else
		right = z[i*N + j+1];
	if (j == 1)
	{
		if (rank == 0)
		{
			//same q as above
			z_next[i*N + j-1] = z[i*N + j+1]; //von neumann
			left = z[i*N + j+1];
		}
		else
			left = z_next[i*N + j-1];
	}
	else
		left = z_next[i*N + j-1];
	return (r[i*N + j]-up-left-right-down)/4;
}	
