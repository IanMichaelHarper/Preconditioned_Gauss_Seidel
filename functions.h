#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define h 1 //grid divisor
#define square_length 10 //length of each of the 8 square proc in domain
#define n square_length/h //num grid points each square is divided into
#define N n + 2  //num points held on each proc, 2 for halo

double * b;

double GS(double * z, double * z_next, double * r, int i, int j, int rank);
double Laplacian(double * phi, int i, int j, int rank);
double dot(double * v, double * u);
void print_matrix(double * A);
